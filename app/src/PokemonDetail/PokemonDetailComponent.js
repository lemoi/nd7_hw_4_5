'use strict';

class componentDetailCtrl {

		constructor($routeParams, PokemonsService) {
				self = this;

				this.pokemon = PokemonsService.get({
        		pokemonId: $routeParams['pokemonId']
    		}, function(successResult) {
		        // Окей!
		        self.notfoundError = false;
		        self.pokemonLoaded = true;

		        self.activeTab = 1;
		        self.disableControlTab = true;
		    }, function(errorResult) {
		        // Не окей..
		        self.notfoundError = true;
		        self.pokemonLoaded = true;
		  	});
		}

		deletePokemon(pokemonId) {
		    this.pokemon.$delete({
		        pokemonId: pokemonId
		    }, function(successResult) {
		        // Окей!
		        self.deletionSuccess = true;
		    }, function(errorResult) {
		        // Не окей..
		        self.deletionError = true;
		    });
		}
}

pokemonApp.component('pokemonDetail', {
    templateUrl: './src/PokemonDetail/PokemonDetail.html',
    controller: componentDetailCtrl
});
